const cron = require("node-cron");
const cleanUpSchedules = require("./services/cleanUpSchedules");

// Agendamento da limpeza

cron.schedule("*/30 * * * * *", async()=>{
    try{
        await cleanUpSchedules();
        console.log("Limpeza automática executada");
    }
    catch(erorr){
        console.error("Erro ao executar limpeza automática");
    }
})
